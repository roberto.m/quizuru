#include <vector>
#include <string>
#include <fstream>
#include <sstream>

#include "quiz/util.h"

#include "./quiz/cli_quiz.h"
#include "./quiz/answer/text.h"
#include "./quiz/answer/true_false.h"
#include "./quiz/answer/multiple_choice.h"

#include "./quiz/serializer/question_serializer.h"

int main(void) {
    bool is_running;

    CLI cli;
    QuestionSerializer question_serializer;

    std::vector<Question> default_questions = {
        Question("L' Italia fa parte dell'EU?", new TrueFalseAnswer(true)),
        Question("Chi tra questi si chiama marco?", new MultipleChoiceAnswer({
                    {"Roberto", false},
                    {"Marco", true},
                    {"Francesco", false},
                    {"Matteo", false}
                })),
        Question("Parlami della tua esperienza con PHP", new TextAnswer()),
        Question("Quanto vale, approssimativamente, la velocita della luce in Km/s?", new NumericAnswer(300000)),
        Question("Giocare a CS:GO nel 2022 è ancora sensato?", new TrueFalseAnswer(false)),
    };

    std::vector<Question> questions;

    cli.register_command("/run", [&]() {
        if (questions.size() > 0) {
            CLIQuiz(cli, std::move(questions)).start();
        } else {
            CLIQuiz(cli, std::move(default_questions)).start();
        }
    });

    /*
    cli.register_command("/save", [&](){
        std::ofstream outfile("questions.txt");

        std::string saved_data;
        for (const auto& question : default_questions) {
            saved_data += question_serializer.serialize(&question) + "\n";
        }

        outfile << saved_data;
        outfile.close();
    });

    cli.register_command("/load", [&](){
        std::ofstream infile("questions.txt");
        std::stringstream saved_data;
        saved_data << infile.rdbuf();

        auto serialized_questions = util::str_split(saved_data.str(), "\n");
        for (const auto& q : serialized_questions) {
            questions.push_back(*question_serializer.deserialize(q));
        }
        infile.close();
    });
    */

    cli.register_command("/quit", [&](){
        is_running = false;
    });

    while (!cli.wait_for_command() && is_running);

    return 0;
}
