#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "../util.h"
#include "../interfaces/cli.h"

enum AnswerIdentifier {
    MultipleChoice,
    Text,
    Numeric,
    TrueFalse,
    count
};

class Answer
    : public CLIView {

public:
    Answer() {}
    Answer(const Answer& other) = delete;
    Answer& operator=(const Answer& other) = delete;

    virtual std::string cli_view() const {
        return "Risposta ";
    }

    virtual AnswerIdentifier identifier() const = 0;
    virtual void fill(const std::string& answer) = 0;
    virtual double compare(const Answer& other) const = 0;
};
