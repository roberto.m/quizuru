#pragma once

#include "answer.h"
#include "text.h"
#include "numeric.h"
#include "true_false.h"
#include "multiple_choice.h"

namespace AnswerFactory {
    template<typename AnswerType>
    static AnswerType* new_answer(const AnswerType* copy_from = nullptr) {
        AnswerType* answer;
        if (copy_from != nullptr)  {
            answer = new AnswerType(*copy_from);
        } else {
            answer = new AnswerType();
        }

        return answer;
    }

    static Answer* create_answer(AnswerIdentifier identifier, const Answer* copy_from = nullptr) {
        switch (identifier) {
            case TrueFalse: {
                return new_answer<TrueFalseAnswer>(dynamic_cast<const TrueFalseAnswer*>(copy_from));
            }

            case Text: {
                return new_answer<TextAnswer>(dynamic_cast<const TextAnswer*>(copy_from));
            }

            case Numeric: {
                return new_answer<NumericAnswer>(dynamic_cast<const NumericAnswer*>(copy_from));
            }

            case MultipleChoice: {
                return new_answer<MultipleChoiceAnswer>(dynamic_cast<const MultipleChoiceAnswer*>(copy_from));
            }

            // Shouldn't be possible, in case it happens let the caller handle it
            default: {
                return nullptr;
            }
        }
    }
};
