#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "answer.h"

class NumericAnswer : public Answer {
    int _value;

public: 
    NumericAnswer() {};

    NumericAnswer(const NumericAnswer& other)
        : Answer(), _value(other._value) {};

    NumericAnswer(int answer)
        : _value(answer) {};

    NumericAnswer(const std::string& answer) {
        fill(answer);
    }

    int answer() const {
        return _value;
    }

    virtual AnswerIdentifier identifier() const override;
    virtual void fill(const std::string& answer) override;
    virtual double compare(const Answer& other) const override;
    std::string cli_view() const;
};
