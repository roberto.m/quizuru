#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "answer.h"

class TrueFalseAnswer : public Answer {
    bool _value;

public: 
    TrueFalseAnswer() {};

    TrueFalseAnswer(const TrueFalseAnswer& other)
        : Answer(), _value(other._value) {};

    TrueFalseAnswer(bool answer) : _value(answer) {};

    bool answer() const {
        return _value;
    }
    
    virtual AnswerIdentifier identifier() const override;
    virtual void fill(const std::string& answer) override;
    virtual double compare(const Answer& other) const override;
    std::string cli_view() const;
};
