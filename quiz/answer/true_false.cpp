#include "true_false.h"

void TrueFalseAnswer::fill(const std::string& answer) {
    _value = answer == "vero";
}

double TrueFalseAnswer::compare(const Answer& other) const {
    auto tf_other = dynamic_cast<const TrueFalseAnswer*>(&other);
    return tf_other->answer() == _value ? 1.0f : 0.0f;
}

std::string TrueFalseAnswer::cli_view() const {
    return Answer::cli_view() + "(vero | falso): ";
}

AnswerIdentifier TrueFalseAnswer::identifier() const {
    return TrueFalse;
}
