#pragma once

#include <string>
#include <vector>
#include <utility>
#include <algorithm>

#include "answer.h"
#include "numeric.h"

using Choice = std::pair<std::string, bool>;

class MultipleChoiceAnswer : public Answer {
    std::vector<Choice> _choices;

public: 
    MultipleChoiceAnswer() {}

    MultipleChoiceAnswer(const MultipleChoiceAnswer& other)
        : Answer(), _choices(other._choices) {}

    MultipleChoiceAnswer(const std::vector<Choice>& choices) 
        : _choices(choices) {}

    const std::vector<Choice>& choices() const {
        return _choices;
    }

    virtual AnswerIdentifier identifier() const override;

    /*  
     *  Initializes an incomplete mca, each choice contains an empty string.
     *  param. needs to contain indexes. Each index, tells which choice has to be
     *  set true..
     */ 
    virtual void fill(const std::string& answer) override;
    virtual double compare(const Answer& other) const override;
    std::string cli_view() const;
};
