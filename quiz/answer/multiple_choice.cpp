#include "multiple_choice.h"

void MultipleChoiceAnswer::fill(const std::string& answer) {
    auto answer_indexes = util::str_split(answer, " ");
    std::vector<size_t> indexes;

    for (const auto& index_string : answer_indexes) {
        try {
            size_t index = (size_t) std::stoi(index_string);

            // Remove 1 offset given by user string
            indexes.push_back(index - 1);
        } catch (...) {
            continue;
        }
    }

    size_t items_to_spawn = *std::max_element(indexes.begin(), indexes.end()) + 1;
    _choices.assign(items_to_spawn, {"", false});

    for (const auto& index : indexes) {
        _choices[index].second = true;
    }
}

double MultipleChoiceAnswer::compare(const Answer& other) const {
    double score = 0.0;

    if (other.identifier() != MultipleChoice) {
        return score;
    }

    auto other_mca = dynamic_cast<const MultipleChoiceAnswer*>(&other);
    size_t min_size = std::max(other_mca->choices().size(), _choices.size());

    for (size_t i = 0; i < min_size; i++) {
        if (other_mca->choices()[i].second && _choices[i].second) {
            score += 1.0;
        }
    }

    return score / _choices.size();
}

std::string MultipleChoiceAnswer::cli_view() const {
    std::string view;

    for (size_t i = 0; i < _choices.size(); ++i) {
        view += "\t";
        view += std::to_string(i + 1) + ") ";
        view += _choices[i].first + "\n";
    };

    return Answer::cli_view() + "\n" + view + "(es. 1 2):";
}

AnswerIdentifier MultipleChoiceAnswer::identifier() const {
    return MultipleChoice;
}
