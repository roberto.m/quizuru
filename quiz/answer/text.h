#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "answer.h"

class TextAnswer : public Answer {
    std::string _text;

public: 
    TextAnswer() {}

    TextAnswer(const TextAnswer& other)
        : Answer(), _text(other._text) {}

    TextAnswer(const std::string& answer)
        : _text(answer) {};

    const std::string& text() const {
        return _text;
    }

    virtual AnswerIdentifier identifier() const override;
    virtual void fill(const std::string& answer) override;
    virtual double compare(const Answer& other) const override;
    std::string cli_view() const;
};

