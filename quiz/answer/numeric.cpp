#include "numeric.h"

void NumericAnswer::fill(const std::string& answer) {
    try {
        _value = std::stoi(answer);
    } catch (...) {
        _value = 0;
    }
}

double NumericAnswer::compare(const Answer& other) const {
    auto n_other = dynamic_cast<const NumericAnswer*>(&other);
    return n_other->answer() == _value ? 1.0f : 0.0f;
}

std::string NumericAnswer::cli_view() const {
    return Answer::cli_view() + "(numero intero): ";
}

AnswerIdentifier NumericAnswer::identifier() const {
    return Numeric;
}
