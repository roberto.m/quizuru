#include "text.h"

void TextAnswer::fill(const std::string& answer) {
    _text = answer;
}

double TextAnswer::compare(const Answer& other) const {
    return 1.0f;
}

std::string TextAnswer::cli_view() const {
    return Answer::cli_view() + "(testo): ";
}

AnswerIdentifier TextAnswer::identifier() const {
    return Text;
}
