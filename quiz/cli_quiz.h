#pragma once

#include <vector>
#include <iostream>
#include <string>

#include "cli.h"
#include "quiz.h"
#include "./question/question.h"
#include "./serializer/serializer.h"
#include "./serializer/quiz_serializer.h"

class CLIQuiz {
    CLI _cli;
    Quiz _quiz;
    bool _is_running = true;

    void run();
    void quit();

public:
    CLIQuiz() = default;

    CLIQuiz(CLI cli, std::vector<Question> questions)
        : _quiz(questions), _cli(cli) {}

    void start();
};
