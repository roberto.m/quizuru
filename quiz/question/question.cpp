#include "question.h"

std::string Question::cli_view() const {
    return _question + "\n" + _correct_answer->cli_view();
}
