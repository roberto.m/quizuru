#pragma once

#include <string>
#include <vector>

#include "../answer/answer.h"
#include "../answer/answer_factory.h"
#include "../interfaces/cli.h"

class Question
    : public CLIView {

protected:
    std::string _question;
    Answer* _correct_answer = nullptr;

public:
    Question() {}

    Question(const std::string& question, Answer* correct_answer)
        : _question(question), _correct_answer(correct_answer) {}

    ~Question() {
        if (_correct_answer != nullptr) {
            delete _correct_answer;
        }
    }

    Question(const Question& other) {
        Answer* new_answer = AnswerFactory::create_answer(other.correct_answer()->identifier(), other._correct_answer);
        _question = other._question;
        _correct_answer = new_answer;
    }

    Question& operator=(Question& other) noexcept {
        Answer* new_answer = AnswerFactory::create_answer(other.correct_answer()->identifier(), other._correct_answer);
        _question = other._question;
        _correct_answer = new_answer;
        return *this;
    }

    const std::string& question() const {
        return _question;
    }

    const Answer* correct_answer() const {
        return _correct_answer;
    }

    virtual std::string cli_view() const;
};
