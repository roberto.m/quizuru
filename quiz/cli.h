#pragma once

#include <map>
#include <string>
#include <iostream>
#include <functional>

#include "interfaces/cli.h"

class CLI {
    std::map<std::string, std::function<void()>> _commands;

    std::string get_line() const;

public:
    bool wait_for_command() const;
    std::string ask_user(const CLIView* view) const;
    void tell_user(const std::string& message) const;
    void register_command(const std::string& command_string, std::function<void()> command);
};
