#include "cli_quiz.h"
#include <sstream> 

#include "./serializer/serializer.h"
#include "./serializer/quiz_serializer.h"

void CLIQuiz::run() {
    while(_quiz.has_pending_question()) {
        const CLIView* question = dynamic_cast<const CLIView*>(&(_quiz.current_question()));
        _quiz.reply(_cli.ask_user(question));
    }

    quit();
}

void CLIQuiz::quit() {
    std::stringstream message_stream;
    message_stream << "Punteggio finale: " << _quiz.score() << "/" << _quiz.max_score() << " pts.";
    _cli.tell_user(message_stream.str());
}

void CLIQuiz::start() {
    run();
}
