#pragma once

#include <string>

#include "serializer.h"
#include "../answer/answer.h"

class AnswerSerializer : Serializer<Answer> {
public:
    AnswerSerializer () {
        this->_splitting_token = "@@";
        this->_vector_item_delimiter = "=";
    }

    virtual std::string serialize(const Answer* deserialized) const override;
    virtual Answer* deserialize(const std::string& serialized) const override;
};