#pragma once

#include <string>

#include "serializer.h"
#include "answer_serializer.h"
#include "../question/question.h"

class QuestionSerializer : Serializer<Question> {
    AnswerSerializer _answer_serializer;

public:
    QuestionSerializer() : _answer_serializer(AnswerSerializer()) {
        this->_splitting_token = "§§";
        this->_vector_item_delimiter = "]";
    }

    virtual std::string serialize(const Question* deserialized) const override;
    virtual Question* deserialize(const std::string& serialized) const override;
};
