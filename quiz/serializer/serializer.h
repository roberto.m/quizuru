#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <functional>

#include <iostream>

#include "../util.h"

template<typename T>
class Serializer {
public:
    virtual std::string serialize(const T* deserialized) const = 0;
    virtual T* deserialize(const std::string& serialized) const = 0;

protected:
    std::string _splitting_token;
    std::string _vector_item_delimiter;

    template<typename SomeType>
    static std::string serialize_vector(std::vector<SomeType> items, std::string delimiter = ",") {
        std::stringstream serialized_stream;

        for (size_t i = 0; i < items.size(); ++i) {
            serialized_stream << items[i];
            if (i != items.size() - 1) {
                serialized_stream << delimiter;
            }
        }

        return serialized_stream.str();
    }

    template<typename SomeType>
    static std::vector<SomeType> deserialize_vector(const std::string& serialized_items,
        std::function<SomeType(const std::string&)> converter, std::string delimiter = ",") {

        std::vector<std::string> items = util::str_split(serialized_items, delimiter);
        std::vector<SomeType> deserialized_items;

        for (const auto& item : items) {
            deserialized_items.push_back(converter(item));
        }

        return deserialized_items;
    }

    static std::string opening_token() {
        return "{";
    }

    static std::string closing_token() {
        return "}";
    }

    static bool is_valid_format(std::string maybe_serialized) {
        int balance = 0;
        for (const auto character : maybe_serialized) {
            balance += std::to_string(character) == opening_token()
                ? 1
                : std::to_string(character) == closing_token()
                    ? -1
                    : 0;
        }
        
        return balance == 0;
    }

    static void explode(std::string& serialized) {
        if (is_valid_format(serialized)) {
            serialized.erase(serialized.begin());
            serialized.erase(serialized.end() - 1);
        }
    }
};
