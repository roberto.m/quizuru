
#include <sstream>

#include "question_serializer.h"

#include "../util.h"

std::string QuestionSerializer::serialize(const Question* deserialized) const {
    std::stringstream serialized_stream;

    serialized_stream << Serializer::opening_token();
    serialized_stream << deserialized->question();
    serialized_stream << _splitting_token;

    serialized_stream << _answer_serializer.serialize(deserialized->correct_answer());
    serialized_stream << Serializer::closing_token();

    return serialized_stream.str();
}

Question* QuestionSerializer::deserialize(const std::string& serialized) const {
    std::string to_deserialize = serialized;
    Serializer::explode(to_deserialize);

    auto tokens = util::str_split(to_deserialize, _splitting_token);
    auto correct_answer = _answer_serializer.deserialize(tokens[1]);

    return new Question(tokens[0], correct_answer);
}
