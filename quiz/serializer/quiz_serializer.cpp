#include <sstream>

#include "quiz_serializer.h"

#include "../util.h";

std::string QuizSerializer::serialize(const Quiz* deserialized) const {
    std::stringstream serialized_stream;
    serialized_stream << Serializer::opening_token();

    for (const auto& question_pair : deserialized->questions()) {
        serialized_stream << _question_serializer.serialize(&question_pair.first) << _question_bind_token;
        serialized_stream << _answer_serializer.serialize(question_pair.second) << _splitting_token;
    }

    serialized_stream << Serializer::closing_token();
    return serialized_stream.str();
}

Quiz* QuizSerializer::deserialize(const std::string& serialized) const {
    std::string to_deserialize = serialized;
    Serializer::explode(to_deserialize);

    auto converter = [&](const std::string& item) -> std::pair<Question, Answer*> {
        auto tokens = util::str_split(item, _question_bind_token);

        auto* question_ptr = _question_serializer.deserialize(tokens[0]);
        auto* answer = _answer_serializer.deserialize(tokens[1]);

        Question question = *question_ptr;
        delete question_ptr;

        return { question, answer };
    };

    auto questions = Serializer::deserialize_vector<std::pair<Question, Answer*>>(to_deserialize, converter, _splitting_token);
    return new Quiz(questions);
}
