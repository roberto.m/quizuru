#pragma once

#include <string>
#include <vector>

#include "serializer.h"
#include "answer_serializer.h"
#include "question_serializer.h"
#include "../quiz.h"

class QuizSerializer : public Serializer<Quiz> {
    AnswerSerializer _answer_serializer;
    QuestionSerializer _question_serializer;
    std::string _question_bind_token = "--";

public:
    QuizSerializer()
        : _question_serializer(QuestionSerializer()), _answer_serializer(AnswerSerializer()) {
        this->_splitting_token = "##";
        this->_vector_item_delimiter = "[";
    }

    virtual std::string serialize(const Quiz* deserialized) const override;
    virtual Quiz* deserialize(const std::string& serialized) const override;
};