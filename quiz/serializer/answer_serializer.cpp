#include "answer_serializer.h"

#include <sstream>
#include <string>

#include "../answer/text.h";
#include "../answer/numeric.h";
#include "../answer/true_false.h";
#include "../answer/multiple_choice.h";

#include "../util.h"

std::string AnswerSerializer::serialize(const Answer* deserialized) const {
    std::stringstream serialized_stream;

    serialized_stream << Serializer::opening_token();
    serialized_stream << deserialized->identifier();
    serialized_stream << _splitting_token;

    switch (deserialized->identifier()) {
        case MultipleChoice: {
            auto mca = dynamic_cast<const MultipleChoiceAnswer*>(deserialized);

            std::vector<std::string> serialized_pairs; 
            for (const auto& pair : mca->choices()) {
                serialized_pairs.push_back(pair.first + std::to_string(pair.second));
            }

            serialized_stream << Serializer::serialize_vector(serialized_pairs, _vector_item_delimiter);
            break;
        }

        case Text: {
            auto ta = dynamic_cast<const TextAnswer*>(deserialized);
            serialized_stream << ta->text();
            break;
        }

        case Numeric: {
            auto na = dynamic_cast<const NumericAnswer*>(deserialized);
            serialized_stream << na->answer();
            break;
        }

        case TrueFalse: {
            auto tfa = dynamic_cast<const TrueFalseAnswer*>(deserialized);
            serialized_stream << tfa->answer();
            break;
        }
    }

    serialized_stream << Serializer::closing_token();
    return serialized_stream.str();
}

Answer* AnswerSerializer::deserialize(const std::string& serialized) const {
    std::string to_deserialize = serialized;
    Serializer::explode(to_deserialize);

    auto tokens = util::str_split(to_deserialize, _splitting_token);
    AnswerIdentifier identifier = (AnswerIdentifier) std::stoi(tokens[0]);

    switch (identifier) {
        case MultipleChoice: {
            auto converter = [](const std::string& item) -> Choice {
                std::string choice_label = item.substr(0, item.size() - 2);
                bool is_correct = static_cast<bool>(std::stoi(item.substr(item.size() - 2, item.size() - 1)));

                return {
                    choice_label,
                    is_correct
                };
            };

            std::vector<Choice> pairs = Serializer::deserialize_vector<Choice>(tokens[1], converter, _vector_item_delimiter);

            return (Answer*) new MultipleChoiceAnswer(pairs);
        }

        case Text: {
            return (Answer*) new TextAnswer();
        }

        case Numeric: {
            int answer = std::stoi(tokens[1]);
            return (Answer*) new NumericAnswer(answer);
        }

        case TrueFalse: {
            bool answer = tokens[1] == "1";
            return (Answer*) new TrueFalseAnswer(answer);
        }
    }

    // Not good :(
    return nullptr;
}
