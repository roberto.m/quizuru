#pragma once

#include <string>

class CLIView {
public:
    virtual std::string cli_view() const = 0;
};
