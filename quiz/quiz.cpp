#include <memory>

#include "quiz.h"
#include "./answer/answer_factory.h"

void Quiz::add_question(Question question) {
    _questions.push_back({ question, nullptr });
    _pending++;
}

void Quiz::set_pending_count() {
    _pending = 0;
    for (const auto& question_pair : _questions) {
        if (question_pair.second == nullptr) {
            _pending++;
        }
    }
}

size_t Quiz::next_pending_question_id() const {
    std::vector<size_t> candidates;

    /*
        Iterate from `current_id` to `answers.size() + current_id`, this helps searching the 
        next pending question in a clockwise circular way. Wrapping on `answers.size()` is required to avoid
        going out range and to check values coming after the `current id` then before in such order.
    */
    for (size_t i = _current_question_id + 1; i < _questions.size() + _current_question_id + 1; ++i) {
        size_t id = i % _questions.size();

        if (_questions[id].second == nullptr)  {
            return id;
        }
    }

    return _questions.size() + 1;
}

void Quiz::reply(const std::string& attempt) {
    if (!has_pending_question()) {
        return;
    }

    // An empty string means: Skip question
    if (attempt.size() == 0) {
        _current_question_id = next_pending_question_id();
        return;
    }

    const Question& question = _questions[_current_question_id].first;
    Answer* answer = AnswerFactory::create_answer(question.correct_answer()->identifier());

    if (answer == nullptr) {
        std::cerr << "Couldn't instantiate an answer based on it's identifier.\n";
        exit(1);
    }

    answer->fill(attempt);
    _score += question.correct_answer()->compare(*answer);
    _questions[_current_question_id].second = answer;

    _current_question_id = next_pending_question_id();
    _pending--;
}

const Question& Quiz::current_question() {
    return _questions[_current_question_id].first;
}

