#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

namespace util {
    static std::vector<std::string> str_split(std::string to_split, std::string delimiter) {
        auto token = "";
        size_t pos = 0;
        std::vector<std::string> tokens;

        while ((pos = to_split.find(delimiter)) != std::string::npos) {
            auto s = to_split.substr(0, pos);
            tokens.push_back(s);
            to_split.erase(0, pos + delimiter.length());
        }

        tokens.push_back(to_split);
        return tokens;
    }

    template<typename T>
    static std::string join_vector(std::vector<T> v, const std::string& join_on) {
        std::stringstream joined_vector;
        joined_vector << "[";

        for (size_t i = 0; i < v.size(); ++i) {
            joined_vector << v[i];
            
            if (i < v.size() - 1) {
                joined_vector << join_on << " ";
            }
        }

        joined_vector << "]";
        return joined_vector.str();
    }
};
