#include "cli.h"

bool CLI::wait_for_command() const {
    std::cout << "Inserisci uno dei seguenti comandi: ";
    for(auto const& command : _commands) {
        std::cout << command.first << ", ";
    }
    std::cout << "\n\n";

    std::string command;
    std::getline(std::cin, command);

    size_t count = _commands.count(command);
    if (count) {
        _commands.at(command)();
    } else {
        std::cout << "Non esiste alcun comando: " << command << "std::endl";
    }

    return count > 0;
}

std::string CLI::get_line() const {
    std::string answer;
    std::getline(std::cin, answer);
    std::cout << std::endl;
    return answer;
}

std::string CLI::ask_user(const CLIView *view) const {
    std::cout << view->cli_view();
    return get_line();
}

void CLI::tell_user(const std::string &message) const {
    std::cout << message << std::endl;
}

void CLI::register_command(const std::string& command_string, std::function<void()> command) {
    // Insert only if not present
    if (!_commands.count(command_string)) {
        _commands[command_string] = command;
    }
}
