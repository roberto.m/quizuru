#pragma once

#include <map>
#include <vector>
#include <fstream>
#include <utility>
#include <sstream>
#include <iostream>
#include <filesystem>

#include "./answer/answer.h"
#include "./question/question.h"

class Quiz {
    double _score = 0;
    int _pending = 0;
    size_t _current_question_id = 0;

    std::vector<std::pair<Question, Answer*>> _questions;

    void set_pending_count();
    size_t next_pending_question_id() const;

public:
    Quiz() {}

    Quiz(const std::vector<Question>& questions)
        : _pending(questions.size()) {
            for (const auto& question : questions) {
                _questions.push_back({ question, nullptr });
            }
    }

    Quiz(const std::vector<std::pair<Question, Answer*>>& questions) 
        : _questions(questions) {
            set_pending_count();
    }

    ~Quiz() {
        for (auto& question_pair : _questions) {
            if (question_pair.second != nullptr) {
                delete question_pair.second;
            }
        }
    }

    double score() const {
        return _score;
    }

    double max_score() const {
        return static_cast<double>(_questions.size());
    }

    bool has_pending_question() const {
        return _pending > 0;
    }

    const std::vector<std::pair<Question, Answer*>>& questions() const {
        return _questions;
    }

    void add_question(Question question);
    void reply(const std::string& answer);
    const Question& current_question();
};

