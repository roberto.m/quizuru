progname := quizuru
cxx := clang++
flags := -std=c++17
nowarnings := -Wno-inconsistent-missing-override -Wno-delete-abstract-non-virtual-dtor

answer_file_names := multiple_choice true_false text numeric
answer_dir := $(addprefix quiz/answer/, $(addsuffix .cpp, $(answer_file_names)))
answer_objs := $(addsuffix .o, $(answer_file_names))
question_dir := $(addprefix quiz/question/, question.cpp)
quiz_dir := $(addprefix quiz/, quiz.cpp cli_quiz.cpp cli.cpp)
serializer_dir := $(addprefix quiz/serializer/, quiz_serializer.cpp question_serializer.cpp answer_serializer.cpp)
serializer_obj := quiz_serializer.o question_serializer.o answer_serializer.o

src := quiz/quiz.cpp  main.cpp $(answer_dir) $(question_dir)
objects := main.o quiz.o $(answer_objs) question.o cli_quiz.o cli.o $(serializer_obj)

all: main.o answer.o quiz.o question.o serializer.o
	$(cxx) $(objects) -o $(progname) && rm -f *.o

liner:
	$(cxx) $(src) $(flags) $(nowarnings) -o $(progname) -v

main.o:
	$(cxx) -c main.cpp $(flags) $(nowarnings) -o main.o

quiz.o:
	$(cxx) -c $(quiz_dir) $(flags) $(nowarnings)

serializer.o:
	$(cxx) -c $(serializer_dir) $(flags) $(nowarnings)

answer.o:
	$(cxx) -c $(answer_dir) $(flags) $(nowarnings)

question.o:
	$(cxx) -c $(question_dir) $(flags) $(nowarnings)

.PHONY: clean run linerun

clean:
	rm -f $(progname) *.o

run:
	make && ./$(progname)

linerun:
	make liner && ./$(progname)

